# loop

Simple audio loop machine application to create music

![screenshot](https://gitlab.gnome.org/danigm/loop/raw/main/screenshots/loop02.png)

With a cool new MusicGrid:
![screenshot](https://gitlab.gnome.org/danigm/loop/raw/main/screenshots/loop04.png)
