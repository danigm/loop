# keyboard.py
#
# Copyright 2022 Daniel García Moreno
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import python3_midi as midi
from gi.repository import Gio, Gdk, Gtk


# TODO: create manual keymap
KEYMAP = {
    'C3': 'q',
    'Db3': 'w',
    'D3': 'e',
    'Eb3': 'r',
    'E3': 't',
    'F3': 'y',
    'Gb3': 'u',
    'Ab3': 'i',
    'A3': 'o',
    'Bb3': 'p',
    'B3': 'a',
}


class Spacer(Gtk.Label):
    def __init__(self):
        super().__init__(label='(A)')
        self.set_size_request(80, 150)
        self.get_style_context().add_class('keyboard-key')
        self.get_style_context().add_class('keyboard-key-black')
        self.get_style_context().add_class('keyboard-key-spacer')



class Key(Gtk.Label):
    def __init__(self, note, index, key='', black=False):
        label = note if not black else note + 'b'
        if key:
            label = label + f'\n({key.upper()})'
        super().__init__(label=label)

        self.set_justify(Gtk.Justification.CENTER)

        self.set_size_request(80, 200)
        if black:
            self.set_size_request(80, 150)

        self.note = note
        self.index = index
        self.black = black
        self.clicked = False
        self.key = key

        self.get_style_context().add_class('keyboard-key')
        if self.black:
            self.get_style_context().add_class('keyboard-key-black')

        self.connect_events()

    def connect_events(self):
        click = Gtk.GestureClick()
        click.set_button(Gdk.BUTTON_PRIMARY)
        click.connect('pressed', self.key_clicked)
        click.connect('released', self.key_released)
        self.add_controller(click)

    @property
    def synth(self):
        return Gio.Application.get_default().synth

    @property
    def midi_note(self):
        black = 'b' if self.black else ''
        note_name = f'{self.note}{black}_{self.index}'
        return midi.NOTE_NAME_MAP_FLAT[note_name]

    def key_clicked(self, *args):
        if self.clicked:
            return
        self.clicked = True

        self.get_style_context().add_class('keyboard-key-clicked')
        self.synth.play(self.midi_note)

    def key_released(self, *args):
        self.clicked = False
        self.get_style_context().remove_class('keyboard-key-clicked')
        self.synth.stop(self.midi_note)


@Gtk.Template(resource_path='/net/danigm/loop/keyboard.ui')
class Keyboard(Gtk.Box):
    __gtype_name__ = 'Keyboard'

    white_keys = Gtk.Template.Child()
    black_keys = Gtk.Template.Child()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        notes = 'CDEFGAB'
        black_notes = 'ABDEG'

        key_layout = 'qwertyuiopasdfghjklzxcvbnm'
        keymap = {}

        first_key = True
        for i in range(3, 5):
            for note in notes:
                keyboard_key = key_layout[len(keymap)]
                k = Key(note, i, keyboard_key)
                self.white_keys.append(k)
                keymap[keyboard_key] = k

                # 'Db_0', 'Eb_0', 'Gb_0', 'Ab_0', 'Bb_0',
                if note in black_notes:
                    keyboard_key = key_layout[len(keymap)]
                    k = Key(note, i, keyboard_key, True)
                    self.black_keys.append(k)
                    keymap[keyboard_key] = k
                elif not first_key:
                    self.black_keys.append(Spacer())

                first_key = False

        self.keymap = keymap

        click = Gtk.GestureClick()
        click.set_button(Gdk.BUTTON_PRIMARY)
        click.connect('released', self._grab_focus)
        self.add_controller(click)

        self.connect_keys()

    def _grab_focus(self, *args, **kargs):
        self.grab_focus()

    def connect_keys(self):
        event_controller = Gtk.EventControllerKey()
        event_controller.connect('key-pressed', self.on_key_pressed)
        event_controller.connect('key-released', self.on_key_released)

        self.add_controller(event_controller)

    def on_key_pressed(self, controller, keyval, keycode, state):
        key_name = Gdk.keyval_name(keyval)
        key = self.keymap.get(key_name, None)
        if key:
            key.key_clicked()

    def on_key_released(self, controller, keyval, keycode, state):
        key_name = Gdk.keyval_name(keyval)
        key = self.keymap.get(key_name, None)
        if key:
            key.key_released()
