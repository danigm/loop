# track.py
#
# Copyright 2021 Daniel García Moreno
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
from enum import Enum
from gi.repository import GLib, Gst, Gtk, Gio

from . import config


class TrackState(Enum):
    Empty = 0
    Stopped = 1
    Recording = 2
    Playing = 3



@Gtk.Template(resource_path='/net/danigm/loop/track.ui')
class Track(Gtk.Box):
    __gtype_name__ = 'Track'

    label = Gtk.Template.Child()
    timer = Gtk.Template.Child()
    record = Gtk.Template.Child()
    synth = Gtk.Template.Child()
    play = Gtk.Template.Child()
    loop = Gtk.Template.Child()
    remove = Gtk.Template.Child()

    def __init__(self, number, **kwargs):
        super().__init__(**kwargs)
        self.use_midi = False
        self.state = TrackState.Empty
        self.number = number
        self._pipeline = None
        self._midi_pipeline = None
        self._playbin = None
        self.synth_state = None

        self.record.connect('clicked', self.do_record)
        self.play.connect('toggled', self.do_play)
        self.remove.connect('clicked', self.do_remove)

        self.label.set_text(f'{self.number}')
        self.set_state(TrackState.Empty)
        GLib.timeout_add_seconds(0.2, self.refresh_ui)

    def refresh_ui(self):
        ret = True
        if self.play.get_active():
            pipeline = self.midi_pipeline if self.use_midi else self.playbin
        elif self.record.get_active():
            pipeline = self.pipeline
        else:
            self.timer.set_text('')
            return True

        ret, duration = pipeline.query_position(Gst.Format.TIME)
        if not ret:
            return True

        seconds = duration / Gst.SECOND
        minutes = int(seconds // 60)
        seconds = int(seconds % 60)
        self.timer.set_text(f'{minutes:0>2d}:{seconds:0>2d}')

        return True

    def set_state(self, state):
        self.state = state
        ctx = self.get_style_context()

        classes = ['recording', 'playing', 'empty', 'midi']
        for c in classes:
            ctx.remove_class(c)

        ctx.add_class(state.name.lower())
        if self.use_midi:
            ctx.add_class('midi')

        if self.state == TrackState.Empty:
            self.synth.set_visible(True)
        elif not self.use_midi:
            self.synth.set_visible(False)

        if self.state in [TrackState.Empty, TrackState.Recording]:
            self.record.set_visible(True)

            self.play.set_visible(False)
            self.remove.set_visible(False)
            self.loop.set_visible(False)
        else:
            self.record.set_visible(False)

            self.play.set_visible(True)
            self.remove.set_visible(True)
            self.loop.set_visible(True)

        if self.state == TrackState.Playing:
            self.play.set_icon_name('media-playback-pause-symbolic')
        else:
            self.play.set_icon_name('media-playback-start-symbolic')

    @property
    def pipeline(self):
        if not self._pipeline:
            self._pipeline = self.create_pipeline()
        return self._pipeline

    @property
    def midi_pipeline(self):
        if not self._midi_pipeline:
            self._midi_pipeline = self.create_midi_pipeline()
        return self._midi_pipeline

    @property
    def playbin(self):
        if not self._playbin:
            self._playbin = self.create_playbin()
        return self._playbin

    def create_pipeline(self):
        output = self.get_track_file()
        pipeline = Gst.Pipeline()

        autoaudiosrc = Gst.ElementFactory.make("autoaudiosrc", "autoaudiosrc")
        audioconvert = Gst.ElementFactory.make("audioconvert", "audioconvert")
        vorbisenc = Gst.ElementFactory.make("vorbisenc", "vorbisenc")
        oggmux = Gst.ElementFactory.make("oggmux", "oggmux")
        filesink = Gst.ElementFactory.make("filesink", "filesink")
        filesink.set_property("location", output)
        pipeline.add(autoaudiosrc)
        pipeline.add(audioconvert)
        pipeline.add(vorbisenc)
        pipeline.add(oggmux)
        pipeline.add(filesink)

        autoaudiosrc.link(audioconvert)
        audioconvert.link(vorbisenc)
        vorbisenc.link(oggmux)
        oggmux.link(filesink)

        return pipeline

    def create_midi_pipeline(self):
        midi_file = self.get_midi_file()
        pipeline = Gst.Pipeline()

        # gst-launch-1.0 filesrc location=1.midi ! midiparse ! fluiddec ! pulsesink
        filesrc = Gst.ElementFactory.make("filesrc", "filesrc")
        filesrc.set_property("location", midi_file)
        midiparse = Gst.ElementFactory.make("midiparse", "midiparse")
        fluiddec = Gst.ElementFactory.make("fluiddec", "fluiddec")
        fluiddec.set_property("soundfont", config.DEFAULT_FONT)
        pulsesink = Gst.ElementFactory.make("pulsesink", "pulsesink")

        pipeline.add(filesrc)
        pipeline.add(midiparse)
        pipeline.add(fluiddec)
        pipeline.add(pulsesink)

        filesrc.link(midiparse)
        midiparse.link(fluiddec)
        fluiddec.link(pulsesink)

        bus = pipeline.get_bus()
        bus.add_watch(0, self.midi_bus_call)

        return pipeline

    def midi_bus_call(self, bus, msg):
        if msg.type == Gst.MessageType.EOS:
            if self.loop.get_active():
                self.midi_pipeline.set_state(Gst.State.NULL)
                self.midi_pipeline.set_state(Gst.State.PLAYING)
            else:
                self.play.set_active(False)
        return True

    def create_playbin(self):
        audio_file = self.get_track_file()

        playbin = Gst.ElementFactory.make('playbin', 'playbin')
        playbin.set_property('uri', f'file://{audio_file}')

        bus = playbin.get_bus()
        bus.add_watch(0, self.do_loop)

        return playbin

    def get_file(self, filename):
        datadir = GLib.get_user_data_dir()
        dirname = os.path.join(datadir, 'loop')
        if not os.path.exists(dirname):
            os.mkdir(dirname)

        return os.path.join(dirname, filename)

    def get_track_file(self):
        return self.get_file(f'{self.number}.ogg')

    def get_midi_file(self):
        return self.get_file(f'{self.number}.midi')

    def do_record(self, *args):
        if self.record.get_active():
            self.pipeline.set_state(Gst.State.PLAYING)
            self.set_state(TrackState.Recording)
            self.use_midi = False
        else:
            self.pipeline.set_state(Gst.State.NULL)
            self.set_state(TrackState.Stopped)
            self.loop.set_active(True)
            self.play.set_active(True)

    def play_track(self):
        if self.state == TrackState.Empty:
            return

        self.play.set_active(not self.play.get_active())

    def do_play(self, *args):
        if self.play.get_active():
            if self.use_midi:
                self.midi_pipeline.set_state(Gst.State.NULL)
                self.midi_pipeline.set_state(Gst.State.PLAYING)
            else:
                self.playbin.set_state(Gst.State.NULL)
                self.playbin.set_state(Gst.State.PLAYING)
            self.set_state(TrackState.Playing)
        else:
            self.play.set_active(False)
            if self.use_midi:
                self.midi_pipeline.set_state(Gst.State.NULL)
            else:
                self.playbin.set_state(Gst.State.NULL)
            self.set_state(TrackState.Stopped)

    def do_loop(self, bus, msg):
        if msg.type == Gst.MessageType.EOS:
            if self.loop.get_active():
                self.playbin.set_state(Gst.State.NULL)
                self.playbin.set_state(Gst.State.PLAYING)
            else:
                self.play.set_active(False)
        return True

    def do_remove(self, *args):
        self.record.set_active(False)
        self.play.set_active(False)
        self.use_midi = False

        if self._playbin:
            self._playbin.set_state(Gst.State.NULL)
            self._playbin = None
        if self._pipeline:
            self._pipeline.set_state(Gst.State.NULL)
            self._pipeline = None
        if self._midi_pipeline:
            self._midi_pipeline.set_state(Gst.State.NULL)
            self._midi_pipeline = None

        self.synth_state = None
        self.set_state(TrackState.Empty)

    def select(self):
        if self.state in [TrackState.Empty, TrackState.Recording]:
            self.record.set_active(not self.record.get_active())
            self.do_record()
        else:
            self.play.set_active(not self.play.get_active())
            self.do_play()

    @Gtk.Template.Callback()
    def open_synth(self, *args):
        app = Gio.Application.get_default()
        window = app.get_active_window()
        window.show_track_synth(self.number)

    def set_use_midi(self):
        self.use_midi = True
        self.set_state(TrackState.Stopped)
        self.loop.set_active(True)
        self.play.set_active(True)
