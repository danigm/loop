import threading
from gi.repository import GObject, GLib

from . import fluidsynth

from . import config


class Instrument(GObject.Object):
    __gtype_name__ = 'Instrument'

    def __init__(self, bank, number, name):
        super().__init__()
        self.bank = bank
        self.number = number
        self.name = name

    @GObject.Property
    def prop_name(self):
        return self.name

    def __repr__(self):
        return f'<Instrument {self.bank}-{self.number} {self.name}>'



class FluidSynth(GObject.Object):
    '''
    # fluidsynth messages
    # https://github.com/FluidSynth/fluidsynth/wiki/UserManual

    # midi
    # https://github.com/NFJones/python3-midi
    '''
    def __init__(self):
        super().__init__()

        self.sfid = 0
        self.channel = 1
        self.velocity = 127
        self.instruments = []
        self.loading = True

        self.synth = fluidsynth.Synth()

        self.start(driver='pulseaudio')
        self.set_font(config.DEFAULT_FONT)

    def __del__(self):
        self.synth.delete()

    def __getattr__(self, attr):
        return getattr(self.synth, attr)

    @GObject.Signal
    def loaded(self):
        pass

    def set_font(self, font):
        self.sfid = self.sfload(font)
        self.program_select(self.channel, self.sfid, 0, 0)
        self.load_instruments(self.sfid)

    def select_inst(self, n):
        self.program_change(self.channel, n)

    def play(self, note):
        self.noteon(self.channel, note, self.velocity)

    def stop(self, note):
        self.noteoff(self.channel, note)

    def get_instruments(self, sfid=1):
        instruments = []

        sfont = fluidsynth.fluid_synth_get_sfont_by_id(self.synth.synth, sfid)
        for bank in range(16384):
            for num in range(128):
                preset = fluidsynth.fluid_sfont_get_preset(sfont, bank, num)
                if not preset:
                    continue
                name = fluidsynth.fluid_preset_get_name(preset).decode('ascii')
                instruments.append(Instrument(bank, num, name))
        return instruments

    def load_instruments(self, sfid=1):
        self.instruments = []
        self.loading = True
        GLib.timeout_add(100, self.check_loaded)

        def load_async():
            self.instruments = self.get_instruments(sfid)
            self.loading = False

        t = threading.Thread(target=load_async)
        t.start()

    def check_loaded(self):
        if not self.loading:
            self.emit('loaded')
            return False

        return True
