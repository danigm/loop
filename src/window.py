# window.py
#
# Copyright 2021 Daniel García Moreno
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
from gi.repository import GLib, Gdk, Gtk, Adw
from .track import Track
from . import config


@Gtk.Template(resource_path='/net/danigm/loop/window.ui')
class LoopWindow(Adw.ApplicationWindow):
    __gtype_name__ = 'LoopWindow'

    tracks = Gtk.Template.Child()
    mainbox = Gtk.Template.Child()
    main_stack = Gtk.Template.Child()
    header_stack = Gtk.Template.Child()
    synth_title = Gtk.Template.Child()
    musicgrid = Gtk.Template.Child()

    selected_track = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.set_title("Loop")
        self.selected_track = 1
        self.track_widgets = []

        for i in range(config.TRACKS):
            track = Track(i + 1)
            self.track_widgets.append(track)
            self.tracks.attach(track, i, 0, 1, 1)

        css_provider = Gtk.CssProvider()
        css_provider.load_from_resource("/net/danigm/loop/styles.css")

        display = Gdk.Display.get_default()
        Gtk.StyleContext.add_provider_for_display(display, css_provider,
                Gtk.STYLE_PROVIDER_PRIORITY_USER)

    def get_track(self, number):
        return self.track_widgets[number - 1]

    def show_track_synth(self, track):
        self.selected_track = track
        self.load_track_synth_state(track)
        self.synth_title.set_markup(f'<b>Track {track}</b>')
        self.header_stack.set_visible_child_name('synth')
        self.main_stack.set_visible_child_name('synth')

    def save_track_synth_state(self, track):
        widget = self.get_track(track)
        widget.synth_state = self.musicgrid.get_state()
        self.musicgrid.reset_state()

    def load_track_synth_state(self, track):
        widget = self.get_track(track)
        if widget.synth_state:
            self.musicgrid.load_state(widget.synth_state)

    @Gtk.Template.Callback()
    def show_tracks(self, *args):
        self.save_track_synth_state(self.selected_track)
        self.selected_track = None
        self.header_stack.set_visible_child_name('tracks')
        self.main_stack.set_visible_child_name('tracks')

    @Gtk.Template.Callback()
    def on_synth_ok(self, *args):
        track = self.get_track(self.selected_track)
        output = track.get_midi_file()
        self.musicgrid.save_midi(output)
        track.set_use_midi()
        self.show_tracks()

    def play(self):
        for track in self.track_widgets:
            track.play_track()


class AboutDialog(Gtk.AboutDialog):

    def __init__(self, parent):
        Gtk.AboutDialog.__init__(self)
        self.props.program_name = 'loop'
        self.props.version = config.VERSION
        self.props.authors = ['Daniel García Moreno']
        self.props.copyright = '(C) 2022 Daniel García Moreno'
        self.props.logo_icon_name = 'net.danigm.loop'
        self.set_transient_for(parent)
