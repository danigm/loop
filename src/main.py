# main.py
#
# Copyright 2021 Daniel García Moreno
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import gi

gi.require_version('Gtk', '4.0')
gi.require_version('Gst', '1.0')
gi.require_version('Adw', '1')

from gi.repository import Adw, Gtk, Gio, Gst

from .window import LoopWindow, AboutDialog
from . import config
from . import synth

from .keyboard import Keyboard
from .musicgrid import MusicGrid


class Application(Gtk.Application):
    def __init__(self):
        Gst.init(None)
        Adw.init()
        super().__init__(application_id='net.danigm.loop',
                         flags=Gio.ApplicationFlags.FLAGS_NONE)
        self.synth = synth.FluidSynth()

    def do_activate(self):
        win = self.props.active_window
        if not win:
            win = LoopWindow(application=self)
        self.create_action('play', self.on_play, keybind='<Alt>a')
        self.create_action('experimental', self.on_experimental, keybind='<Alt>e')
        self.create_action('about', self.on_about_action)
        self.create_action('quit', self.on_quit, keybind=f'<Alt>q')
        for i in range(config.TRACKS):
            self.create_action(f'select-track-{i+1}',
                               self.on_select_track,
                               i,
                               keybind=f'<Alt>{i+1}')
        win.present()

    def on_select_track(self, widget, _, track):
        win = self.props.active_window
        win.select_track(track)

    def on_about_action(self, widget, _):
        about = AboutDialog(self.props.active_window)
        about.present()

    def on_experimental(self, widget, _):
        experimental = Gtk.Window(title='Experimental Widgets')
        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)

        box.append(Gtk.Label(label='MIDI Keyboard'))
        box.append(Keyboard())

        box.append(Gtk.Label(label='Music Grid'))
        box.append(MusicGrid())

        clamp = Adw.Clamp()
        clamp.set_child(box)
        experimental.set_child(clamp)
        experimental.show()

    def on_play(self, *args):
        win = self.get_active_window()
        win.play()

    def on_quit(self, widget, _):
        self.quit()

    def create_action(self, name, callback, *args, keybind=None):
        """ Add an Action and connect to a callback """
        action = Gio.SimpleAction.new(name, None)
        action.connect("activate", callback, *args)
        if keybind:
            self.set_accels_for_action(f'app.{name}', [keybind])

        self.add_action(action)


def main(version):
    app = Application()
    return app.run(sys.argv)
