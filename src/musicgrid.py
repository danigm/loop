# musicgrid.py
#
# Copyright 2022 Daniel García Moreno
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import python3_midi as midi
from . import synth
from gi.repository import Gio, GLib, Gtk


SCALES = {
    'classic': [
        'B_3', 'Db_4', 'Gb_4', 'Ab_4',
        'Db_5', 'Eb_5', 'E_5', 'Ab_5',
        'B_5', 'Db_6', 'Gb_6', 'Ab_6'
    ],
    'pentatonic': [
        'C_4', 'D_4', 'E_4', 'G_4',
        'A_4', 'C_5', 'D_5', 'E_5',
        'G_5', 'A_5', 'C_6', 'D_6',
    ],
    'chromatic': [
        'C_5', 'Db_5', 'D_5', 'Eb_5',
        'E_5', 'F_5', 'Gb_5', 'G_5',
        'Ab_5','A_5', 'Bb_5', 'B_5',
    ],
    'major': [
        'C_4', 'D_4', 'E_4', 'F_4',
        'G_4', 'A_4', 'B_4', 'C_5',
        'D_5', 'E_5', 'F_5', 'G_5',
    ],
    'harmonic_minor': [
        'A_4', 'B_4', 'C_5', 'D_5',
        'E_5', 'F_5', 'Ab_5', 'A_5',
        'B_5', 'C_6', 'D_6', 'E_6',
    ],
}


class SynthState:
    def __init__(self):
        self.rows = 0
        self.scale = 'classic'
        self.instrument = 0
        self.tempo = 500
        self.grid = []


class NoteWidget(Gtk.ToggleButton):
    def __init__(self, note, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.note = note

        self.handler = self.connect('toggled', self.play)

    @property
    def synth(self):
        return Gio.Application.get_default().synth

    @property
    def midi_note(self):
        return midi.NOTE_NAME_MAP_FLAT[self.note]

    def play(self, *args):
        self.note_on()
        GLib.timeout_add(500, self.note_off)

    def note_on(self):
        self.synth.play(self.midi_note)

    def note_off(self):
        self.get_style_context().remove_class('cell-playing')
        self.synth.stop(self.midi_note)
        return False


@Gtk.Template(resource_path='/net/danigm/loop/musicgrid.ui')
class MusicGrid(Gtk.Box):
    __gtype_name__ = 'MusicGrid'

    grid = Gtk.Template.Child()
    play_button = Gtk.Template.Child()
    rows = Gtk.Template.Child()
    scale = Gtk.Template.Child()
    instrument_widget = Gtk.Template.Child()
    tempo_spinner = Gtk.Template.Child()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.tempo = 500
        self.grid_size = 0

        self.scale.set_active(0)
        self.create_grid()

        self.playing = False
        self.current_row = 0
        self.instrument = 0

        if not self.synth.loading:
            self.fill_instrument_combo()
        else:
            self.synth.connect('loaded', self.fill_instrument_combo)

    @property
    def synth(self):
        return Gio.Application.get_default().synth

    @property
    def nrows(self):
        return self.rows.get_value_as_int()

    def get_scale(self):
        current = self.scale.get_active_id()
        return SCALES[current]

    def fill_instrument_combo(self, *args):
        model = Gio.ListStore()
        for i in self.synth.instruments:
            if i.bank:
                continue
            iter = model.append(i)

        # Read: https://blog.gtk.org/2020/06/07/scalable-lists-in-gtk-4/
        factory = Gtk.SignalListItemFactory()
        factory.connect('setup', self.instrument_setup)
        factory.connect('bind', self.instrument_bind)

        # TODO: Add a GtkPropertyExpression. It's not possible right now
        # because python bindings does not know how to create types that
        # inherit from GTypeInstance directly

        self.instrument_widget.set_factory(factory)
        self.instrument_widget.set_model(model)

    def instrument_setup(self, factory, item):
        item.set_child(Gtk.Label())

    def instrument_bind(self, factory, item):
        label = item.get_child()
        instrument = item.get_item()
        label.set_text(instrument.name)

    def create_grid(self):
        for r in range(self.nrows):
            self.add_row()

    def play_row(self, button, row):
        for c, note in enumerate(self.get_scale()):
            w = self.grid.get_child_at(c + 1, row)
            if w.props.active:
                w.play()

    @Gtk.Template.Callback()
    def on_play(self, button):
        self.playing = not self.playing
        if self.playing:
            self.play_button.set_icon_name('media-playback-stop-symbolic')
            self.play()
        else:
            self.stop()

    @Gtk.Template.Callback()
    def on_rows_changed(self, button):
        diff = self.grid_size - self.nrows
        if diff < 0:
            for i in range(abs(diff)):
                self.add_row()
        else:
            for i in range(diff):
                self.remove_row()

    @Gtk.Template.Callback()
    def on_tempo_changed(self, button):
        self.tempo = self.tempo_spinner.get_value()

    @Gtk.Template.Callback()
    def on_scale_changed(self, combo):
        if not self.grid_size:
            return

        self.stop()
        for i in range(self.grid_size):
            self.grid.remove_row(0)
        self.grid_size = 0
        self.create_grid()

    @Gtk.Template.Callback()
    def on_instrument_changed(self, combo, item):
        if not combo.props.selected_item:
            return
        self.instrument = combo.props.selected_item.number
        self.synth.select_inst(self.instrument)

    def add_row(self):
        r = self.grid_size
        play_button = Gtk.Button(icon_name='media-playback-start-symbolic')
        play_button.connect('clicked', self.play_row, r)
        self.grid.attach(play_button, 0, r, 1, 1)

        for c, note in enumerate(self.get_scale()):
            child = NoteWidget(note)
            self.grid.attach(child, c + 1, r, 1, 1)
        self.grid_size += 1

    def remove_row(self):
        self.stop()
        self.grid_size -= 1
        self.grid.remove_row(self.grid_size)

    def play(self):
        prev = (self.current_row - 1) % self.nrows
        button = self.grid.get_child_at(0, prev)
        button.get_style_context().remove_class('suggested-action')

        self.play_row(None, self.current_row)

        button = self.grid.get_child_at(0, self.current_row)
        button.get_style_context().add_class('suggested-action')

        self.current_row = (self.current_row + 1) % self.nrows

        if self.playing:
            GLib.timeout_add(self.tempo, self.play)

        return False

    def unselect_all_play(self):
        for i in range(self.grid_size):
            button = self.grid.get_child_at(0, i)
            button.get_style_context().remove_class('suggested-action')

    def stop(self):
        self.play_button.set_icon_name('media-playback-start-symbolic')
        self.unselect_all_play()
        self.playing = False
        self.current_row = 0

    def load_state(self, state):
        self.instrument_widget.set_selected(state.instrument)
        self.rows.set_value(state.rows)
        self.scale.set_active(state.scale)
        self.tempo_spinner.set_value(state.tempo)

        for r in range(self.nrows):
            for c, note in enumerate(self.get_scale()):
                button = self.grid.get_child_at(c + 1, r)
                with button.handler_block(button.handler):
                    button.set_active(state.grid[r][c])

    def get_state(self):
        state = SynthState()
        state.rows = self.rows.get_value()
        state.instrument = self.instrument_widget.get_selected()
        state.scale = self.scale.get_active()
        state.tempo = self.tempo

        for r in range(self.nrows):
            row = []
            for c, note in enumerate(self.get_scale()):
                button = self.grid.get_child_at(c + 1, r)
                row.append(button.get_active())
            state.grid.append(row)

        return state

    def reset_state(self):
        self.stop()

        self.rows.set_value(8)
        self.scale.set_active(0)
        self.instrument_widget.set_selected(0)
        self.tempo_spinner.set_value(500)

        for r in range(self.nrows):
            for c, note in enumerate(self.get_scale()):
                button = self.grid.get_child_at(c + 1, r)
                with button.handler_block(button.handler):
                    button.set_active(False)

    def s_to_tick(self, pattern, seconds):
        '''
        Converts seconds to MIDI ticks

        FIXME: this could not work correctly, I don't understand yet how the
        MIDI tempo is working and this calculation is based on my limited
        understanding of it, based on what I've read here:

        https://github.com/NFJones/python3-midi#side-note-what-is-a-midi-tick
        '''

        # 60000 / (BPM * PPQ)
        tick_ms = 60_000 / (120 * pattern.resolution)
        tick_s = tick_ms / 1000
        return int(seconds // tick_s)

    def save_midi(self, path):
        pattern = midi.Pattern(resolution=220)
        track = midi.Track()
        pattern.append(track)

        # Set instrument
        track.append(midi.ProgramChangeEvent(tick=0, value=self.instrument))

        t = self.s_to_tick(pattern, self.tempo / 1000)
        for r in range(self.nrows):
            on_t = 0
            for c, note in enumerate(self.get_scale()):
                button = self.grid.get_child_at(c + 1, r)
                if button.get_active():
                    track.append(midi.NoteOnEvent(tick=on_t, velocity=127, pitch=button.midi_note))
                    on_t = 0
            track.append(midi.MarkerEvent(tick=on_t))

            off_t = t
            for c, note in enumerate(self.get_scale()):
                button = self.grid.get_child_at(c + 1, r)
                if button.get_active():
                    track.append(midi.NoteOffEvent(tick=off_t, pitch=button.midi_note))
                    off_t = 0
            track.append(midi.MarkerEvent(tick=off_t))

        # End of track
        track.append(midi.EndOfTrackEvent(tick=1))
        midi.write_midifile(path, pattern)
